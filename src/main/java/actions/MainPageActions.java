package actions;

import com.codeborne.selenide.SelenideElement;
import pages.MainPage;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.codeborne.selenide.Condition.disappear;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class MainPageActions {
    MainPage page = new MainPage();

    public void fromFieldClick() {
        page.fromField.click();
    }

    public void selectTbilisi(){
        for (SelenideElement loc : page.locations) {
            if (loc.getText().contains("თბილისი")) {
                loc.click();
                break;


            }

        }

    }

    public void toFieldClick() {
        page.toField.click();
    }

    public void selectBatumi(){
        for (SelenideElement loc : page.destination) {
            if (loc.getText().contains("ბათუმი")) {
                loc.click();
                break;


            }

        }
    }
    public void departureFieldClick() {
        page.departureField.click();
    }

    public void selectFirstAvailableDepartureDate() {
        selectAvailableDate(5); // Select the third available date for departure
        waitForPageToLoad(); // Wait for the page to load after selecting the date
    }

    public void clickReturnField() {
        page.addReturnField.click();
    }

    public void selectFirstAvailableReturnDate() {
        selectAvailableDate(6); // Select the fourth available date for return
        waitForPageToLoad(); // Wait for the page to load after selecting the date
    }

    public void clickSearchButton() {
        page.searchButton.click();
        waitForPageToLoad(); // Wait for the page to load after clicking the search button
    }

    private void selectLocation(String locationName) {
        List<SelenideElement> locations = $$(".react-datepicker__day");
        locations.stream()
                .filter(loc -> loc.getText().contains(locationName))
                .findFirst()
                .ifPresent(SelenideElement::click);
    }

    private void selectAvailableDate(int dateIndex) {
        LocalDate currentDate = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d-ე LLL, yyyy");

        while (true) {
            List<SelenideElement> availableDates = $$(".react-datepicker__day:not(.react-datepicker__day--disabled)");
            if (availableDates.size() >= dateIndex) {
                availableDates.get(dateIndex - 1).click(); // Select the desired available date
                break;
            }


            $(".react-datepicker__navigation--next").click();
        }
    }

    private void waitForPageToLoad() {
        $(".page-loader").should(disappear); // Assuming a page loader element is displayed while the page loads
    }
    public void returnToMainPage(){
        page.returnToMainPage.click();
    }
}