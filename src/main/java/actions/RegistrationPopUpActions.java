package actions;

import data.UserRegistrationData.UserRegistrationData;
import pages.RegistrationPopUp;
import utility.HelperFunctions;

public class RegistrationPopUpActions {
    RegistrationPopUp registrationPopUp = new RegistrationPopUp();
    HelperFunctions helperFunctions = new HelperFunctions();
    UserRegistrationData data = new UserRegistrationData();
    public void enterMail(){
        registrationPopUp.emailField.click();
        registrationPopUp.emailField.setValue(data.Email);


    }
    public void submitRegistration(){
        registrationPopUp.registerButton.click();

    }

}
