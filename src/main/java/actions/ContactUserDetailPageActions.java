package actions;

import data.UserContactData.UserContactData;
import pages.ContactUserDetailsPage;

public class ContactUserDetailPageActions {
    UserContactData data = new UserContactData();
    ContactUserDetailsPage page = new ContactUserDetailsPage();

    public void fillUserEmail(){

        page.userEmail.setValue(data.userEmail);
    }


    public void fillUserPhoneNumber(){
        page.userPhoneNumber.setValue(data.userPhoneNumber);

    }

    public void submitUserContact(){
        page.userContactSubmitButton.click();
    }
    public void copyPromo(){
        page.promoCode.click();
        page.scrollPromoField.click();
        page.enterPromoField.setValue("freeservice");
    }
    public void submitPromo(){
        page.submitPromo.click();
    }


}
