package actions;

import data.UserLogInData.DataForRestore;
import data.UserLogInData.UserLogInData;
import pages.LogInPopUp;
import utility.HelperFunctions;

public class LogInPopUpActions {
    LogInPopUp login = new LogInPopUp();
    HelperFunctions helperFunctions = new HelperFunctions();
    UserLogInData data = new UserLogInData();
    DataForRestore restoreData = new DataForRestore();
    public void enterEmail(){
       // helperFunctions.switchToNewWindow();
        login.emailFieldInLogIn.click();
        login.emailFieldInLogIn.setValue(data.Email);
       // helperFunctions.switchBack();
    }
    public void enterPassword(){
        //helperFunctions.switchToNewWindow();
        login.passwordFieldInLogIn.click();
        login.passwordFieldInLogIn.setValue(data.Password);
       // helperFunctions.switchBack();
    }
    public void signInButton(){
        login.signInButtonInLogIn.click();
    }
    public void showAndHidePassword(){
        login.hidePasswordInLogIn.click();
    }
    public void restorePasswordOpen() throws InterruptedException {
        login.restoreInLogIn.click();


    }
    public void enterMail (){
        login.enterMailForRestore.click();
        login.enterMailForRestore.setValue(restoreData.Email);
    }
    public void submitRestore(){
        login.restoreMailSubmit.click();


    }
    public void finalSubmit(){
        login.finalSubmit.click();
    }
}
