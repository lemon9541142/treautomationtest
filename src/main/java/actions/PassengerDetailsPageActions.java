package actions;

import data.PassengersDetailsData.PassengersDetailsData;
import pages.PassengerDetailsPage;

public class PassengerDetailsPageActions {
    PassengerDetailsPage page = new PassengerDetailsPage();
    PassengersDetailsData data = new PassengersDetailsData();

    public void fillUserNameField () {
        page.userName.setValue(data.userName);

    }



    public void fillUserLastNameField(){

        page.userLastName.setValue(data.userLastName);
    }


    public void fillUserIdField(){
        page.userId.setValue(data.userId);
    }

    public void submitUserDetails(){
        page.submitUserDetails.click();
    }


}
